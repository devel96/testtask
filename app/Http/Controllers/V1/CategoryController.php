<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Repositories\V1\CategoryRepository;
use App\Http\Requests\V1\CategoryRequest;
use App\Http\Requests\V1\GetCategoryRequest;
use App\Models\V1\Category;
use App\Models\V1\Status;

class CategoryController extends Controller
{
    /**
     * Добавляем класс репозитории для данного контроллера
     *
     * @return string
     */
    public function repositoryClass(): string
    {
        return CategoryRepository::class;
    }

    /**
     * @param GetCategoryRequest $request
     * @return array
     */
    public function index(GetCategoryRequest $request)
    {
        return $this->repository->get([
            'limit' => $request->input('limit'),
            'offset' => $request->input('offset'),
        ]);
    }

    /**
     * @param CategoryRequest $request
     * @return array
     */
    public function store(CategoryRequest $request)
    {
        return Status::get($this->repository->add([
            'name' => $request->input('name')
        ]));
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public function show(Category $category)
    {
        return $this->repository->getById($category->id);
    }

    /**
     * @param CategoryRequest $request
     * @param Category $category
     * @return array
     */
    public function update(CategoryRequest $request, Category $category)
    {
        return Status::get($this->repository->update($category->id, [
            'name' => $request->input('name')
        ]));
    }

    /**
     * @param Category $category
     * @return array
     */
    public function destroy(Category $category)
    {
        return Status::get($this->repository->remove($category->id));
    }
}
