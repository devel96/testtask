<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;

class GetProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:50',
            'price_min' => 'integer',
            'price_max' => 'integer',
            'category_id' => 'integer',
            'category_name' => 'string|max:50',
            'published' => 'status',
            'not_deleted' => 'status'
        ];
    }

    public function messages()
    {
        return [
          'published.status' => 'The value should be (string) true or false',
          'not_deleted.status' => 'The value should be (string) true or false',
        ];
    }
}
