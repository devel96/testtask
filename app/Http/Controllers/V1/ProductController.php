<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\RepositoryClassInterface;
use App\Http\Repositories\V1\ProductRepository;
use App\Http\Requests\V1\GetProductRequest;
use App\Http\Requests\V1\ProductRequest;
use App\Http\Requests\V1\UpdateProductRequest;
use App\Models\V1\Product;
use App\Models\V1\Status;

class ProductController extends Controller
{
    /**
     * Добавляем класс репозитории для данного контроллера
     *
     * @return string
     */
    public function repositoryClass(): string
    {
        return ProductRepository::class;
    }

    /**
     * @param GetProductRequest $request
     * @return array
     */
    public function index(GetProductRequest $request)
    {
        return $this->repository->get([
            'name' => $request->input('name'),
            'price_min' => $request->input('price_min'),
            'price_max' => $request->input('price_max'),
            'category_id' => $request->input('category_id'),
            'category_name' => $request->input('category_name'),
            'published' => $request->input('published'),
            'not_deleted' => $request->input('not_deleted')
        ]);
    }

    /**
     * @param ProductRequest $request
     * @return array
     */
    public function store(ProductRequest $request)
    {
        return Status::get(
            $this->repository->add([
                'name' => $request->input('name'),
                'price' => $request->input('price'),
                'categories' => json_decode($request->input('categories'))
            ])
        );
    }

    /**
     * @param UpdateProductRequest $request
     * @param Product $product
     * @return array
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        return Status::get($this->repository->update($product->id, [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'categories' => json_decode($request->input('categories'))
        ]));
    }

    /**
     * @param Product $product
     * @return array
     */
    public function destroy(Product $product)
    {
        return Status::get($this->repository->remove($product->id));
    }
}
