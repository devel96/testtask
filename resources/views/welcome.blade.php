<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <style>
        @import url("https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700");

        *, *:before, *:after {
            box-sizing: border-box;
        }

        body {
            padding: 24px;
            font-family: 'Source Sans Pro', sans-serif;
            margin: 0;
        }

        h1, h2, h3, h4, h5, h6 {
            margin: 0;
        }

        .container {
            max-width: 1000px;
            margin-right: auto;
            margin-left: auto;
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        .table {
            width: 100%;
            border: 1px solid #EEEEEE;
        }

        .table-header {
            display: flex;
            width: 100%;
            background: #000;
            padding: 18px 0;
        }

        .table-row {
            display: flex;
            width: 100%;
            padding: 18px 0;
        }

        .table-row:nth-of-type(odd) {
            background: #EEEEEE;
        }

        .table-data, .header__item {
            flex: 1 1 20%;
            text-align: center;
        }

        .header__item {
            text-transform: uppercase;
        }

        .filter__link {
            color: white;
            text-decoration: none;
            position: relative;
            display: inline-block;
            padding-left: 24px;
            padding-right: 24px;
        }

        .filter__link::after {
            content: '';
            position: absolute;
            right: -18px;
            color: white;
            font-size: 12px;
            top: 50%;
            transform: translateY(-50%);
        }

        .filter__link.desc::after {
            content: '(desc)';
        }

        .filter__link.asc::after {
            content: '(asc)';
        }

    </style>
</head>
<body class="antialiased">
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
    <div class="max-w-12xl mx-auto sm:px-12 lg:px-12">

        <div class="container">

            <div class="table">
                <div class="table-header">
                    <div class="header__item"><a id="name" class="filter__link">Repository</a></div>
                    <div class="header__item"><a id="name" class="filter__link">Route</a></div>
                    <div class="header__item"><a id="wins" class="filter__link filter__link--number">Method</a></div>
                    <div class="header__item"><a id="draws" class="filter__link filter__link--number">Body</a>
                    </div>
                    <div class="header__item"><a id="losses" class="filter__link filter__link--number">Headers</a></div>
                </div>
                <div class="table-content">
                    @foreach($data as $row)
                        <div class="table-row"
                            @if($row['repository'] === 'product')
                                style="color: darkred"
                            @endif>
                            <div class="table-data">{{ $row['repository'] }}</div>
                            <div class="table-data" style="text-align: left">{{ '/api/v1/' . $row['route'] }}</div>
                            <div class="table-data">{{ $row['method'] }}</div>
                            <div class="table-data" style="text-align: left">
                                <ul>
                                    @foreach($row['parameters'] as $name => $type)
                                        <li>{{ $name . ' - '  . $type }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="table-data">Accept: application/json</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


    </div>
</div>
</body>
</html>
