<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $categories = [
        [
            'repository' => 'category',
            'route' => 'categories/{category}',
            'method' => 'GET',
            'parameters' => []
        ],
        [
            'repository' => 'category',
            'route' => 'categories',
            'method' => 'GET',
            'parameters' => [
                'limit' => 'integer',
                'offset' => 'integer'
            ]
        ],
        [
            'repository' => 'category',
            'route' => 'categories',
            'method' => 'POST',
            'parameters' => [
                'name' => 'string'
            ]
        ],
        [
            'repository' => 'category',
            'route' => 'categories/{category}',
            'method' => 'PUT',
            'parameters' => [
                'name' => 'string'
            ]
        ],
        [
            'repository' => 'category',
            'route' => 'categories/{category}',
            'method' => 'DELETE',
            'parameters' => []
        ],
    ];

    $products = [
        [
            'repository' => 'product',
            'route' => 'products',
            'method' => 'GET',
            'parameters' => [
                'name' => 'name',
                'price_min' => 'integer',
                'price_max' => 'integer',
                'category_id' => 'integer',
                'category_name' => 'string',
                'published' => 'boolean string (true or false)',
                'not_deleted' => 'boolean string (true or false)'
            ]
        ],
        [
            'repository' => 'product',
            'route' => 'products',
            'method' => 'POST',
            'parameters' => [
                'name' => 'string',
                'price' => 'integer',
                'categories' => 'json encoded array with category ids [1,2,3], min 2 max 10'
            ]
        ],
        [
            'repository' => 'product',
            'route' => 'products/{product}',
            'method' => 'PUT',
            'parameters' => [
                'name' => 'string',
                'price' => 'integer',
                'categories' => 'json encoded array with category ids [1,2,3], min 2 max 10'
            ]
        ],
        [
            'repository' => 'product',
            'route' => 'products/{product}',
            'method' => 'DELETE',
            'parameters' => []
        ]
    ];

    $data = array_merge($categories, $products);

    return view('welcome', compact('data'));
});
