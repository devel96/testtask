<?php


namespace App\Http\Repositories\V1;


use App\Http\Repositories\IRepository;
use App\Models\V1\Category;
use App\Models\V1\ProductCategory;
use App\Models\V1\Status;

class CategoryRepository implements IRepository
{
    public const DEFAULT_OFFSET = 0;
    public const DEFAULT_LIMIT = 1000;

    /**
     * Добавляем новую категорию
     *
     * @param array $params
     * @return mixed|void
     *
     */
    public function add(array $params): bool
    {
        return Category::create([
            'name' => $params['name'],
            'status' => Status::ACTIVE
        ]) ? true : false;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function remove(int $id): bool
    {
        if (!ProductCategory::where('category_id', $id)->get()->count()) {
            return Category::destroy($id);
        }

        return false;
    }

    /**
     * @param array $params
     * @return array
     */
    public function get(array $params = []): array
    {
        $params['where'][] = ['status', Status::ACTIVE];

        return Category::select(['id', 'name', 'status'])
            ->where($params['where'])
            ->offset($params['offset'] ?? self::DEFAULT_OFFSET)
            ->limit($params['limit'] ?? self::DEFAULT_LIMIT)
            ->get()->toArray();
    }

    /**
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        return Category::find($id)->toArray();
    }

    /**
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function update(int $id, array $params): bool
    {
        $category = Category::find($id);

        foreach ($params as $column => $value) {
            if ($value) {
                $category[$column] = $value;
            }
        }

        return $category->save();
    }

    /**
     * @param array $ids
     * @return int
     */
    public function countByIds(array $ids): int
    {
        return Category::select(['id'])
            ->whereIn('id', $ids)
            ->get()->count();
    }
}
