<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'unique:products|max:50',
            'categories' => 'product_category_validation'
        ];
    }

    public function messages()
    {
        return [
            'categories.product_category_validation' => 'Please use correct json data (array of category ids min:2; max:10)'
        ];
    }

}
