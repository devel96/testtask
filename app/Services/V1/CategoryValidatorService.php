<?php

namespace App\Services\V1;

use App\Http\Repositories\V1\CategoryRepository;
use Illuminate\Validation\Validator;
use Psy\Util\Json;

class CategoryValidatorService extends Validator
{
    private const MIN_CATEGORIES_COUNT = 2;
    private const MAX_CATEGORIES_COUNT = 10;

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateProductCategoryValidation($attribute, $value, $parameters)
    {
        if ($data = json_decode($value)) {
            if(!is_array($data)) {
                return false;
            }

            $count = count($data);

            if ($count < self::MIN_CATEGORIES_COUNT || $count > self::MAX_CATEGORIES_COUNT) {
                return false;
            }

            // Используем композицию
            $repository = new CategoryRepository();

            // Возвращаем true если количество переданных категорий идентичен с БД
            return $count == $repository->countByIds($data);
        }

        return false;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateStatus($attribute, $value, $parameters)
    {
       return in_array($value, ['true', 'false']);
    }
}

