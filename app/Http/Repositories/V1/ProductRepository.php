<?php


namespace App\Http\Repositories\V1;


use App\Http\Repositories\IRepository;
use App\Models\V1\Product;
use App\Models\V1\ProductCategory;
use App\Models\V1\Status;
use Illuminate\Support\Facades\DB;

class ProductRepository implements IRepository
{
    public const DEFAULT_OFFSET = 0;
    public const DEFAULT_LIMIT = 1000;

    /**
     * @param array $params
     * @return bool
     */
    public function add(array $params): bool
    {
        try {
            DB::transaction(function () use ($params) {
                $product = Product::create([
                    'name' => $params['name'],
                    'price' => $params['price'],
                    'status' => Status::ACTIVE
                ]);

                $insertData = [];
                foreach ($params['categories'] as $category) {
                    $insertData[] = ['product_id' => $product->id, 'category_id' => $category];
                }

                ProductCategory::insert($insertData);
            });
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function remove(int $id): bool
    {
        return Product::find($id)->update([
            'status' => Status::DELETED
        ]);
    }

    /**
     * В Методе конечно мог включить отдельный класс
     * для построения query (как searchModel в Yii)
     *
     * Поскольку в тз не описаны такие поля как offset и limit
     * я этот функционал реализовал только в CategoryRepository/get
     *
     * @param array $params
     * @return array
     */
    public function get(array $params = []): array
    {
        $queryBuilder = Product::select(
            'products.id as id',
            'products.name as name',
            'products.price as price',
            'products.status as status',
            'categories.id as category_id',
            'categories.name as category_name',
            )
            ->join('products_categories', 'products.id', '=', 'products_categories.product_id')
            ->join('categories', 'categories.id', '=', 'products_categories.category_id');


        // Добавляем нужные параметры в queryBuilder
        if ($params['name']) {
            $queryBuilder->where('products.name', 'like', '%' . $params['name'] . '%');
        }

        if ($params['price_min']) {
            $queryBuilder->where('products.price', '>=', $params['price_min']);
        }

        if ($params['price_max']) {
            $queryBuilder->where('products.price', '<=', $params['price_max']);
        }

        if ($params['category_id']) {
            $queryBuilder->where('categories.id', $params['category_id']);
        }

        if ($params['category_name']) {
            $queryBuilder->where('categories.name', 'like', '%' . $params['category_name'] . '%');
        }

        if ($params['published']) {
            $queryBuilder->where('products.status', Status::getNumeric($params['published']));
        }

        if ($params['not_deleted']) {
            $queryBuilder->where('products.status', Status::getDeletedOperator($params['not_deleted']),
                Status::DELETED);
        }

        return $queryBuilder->get()->toArray();
    }

    /**
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function update(int $id, array $params): bool
    {
        try {
            DB::transaction(function () use ($id, $params) {
                $product = Product::find($id);

                foreach ($params as $column => $value) {
                    if ($column !== 'categories' && $value) {
                        $product[$column] = $value;
                    }
                }

                $product->save();

                /**
                 * Чтобы добавить все необходимые категории
                 * в этот раз вместо батч инсерта сделаем обычний инсерт
                 */
                if (isset($params['categories'])) {
                    foreach ($params['categories'] as $category) {
                        ProductCategory::firstOrNew([
                            'product_id' => $id,
                            'category_id' => $category
                        ]);
                    }
                }

            });
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

}
