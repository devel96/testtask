<?php


namespace App\Http\Repositories;

/**
 * Interface IRepository
 * @package App\Http\Repositories
 *
 * Интерфейс используется для контроля типов
 *
 */
interface IRepository
{
    /**
     * @param array $params
     * @return bool
     */
    public function add(array $params): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function remove(int $id): bool;

    /**
     * @param array $params
     * @return array
     */
    public function get(array $params = []): array;

    /**
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function update(int $id, array $params): bool;
}
