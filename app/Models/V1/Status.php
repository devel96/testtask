<?php


namespace App\Models\V1;

use http\Env\Response;

/**
 * В классе сохраняем все возможные статусы
 * Class Status
 * @package App\Models\V1
 */
class Status
{
    // Статусы для работы с БД
    public const NOT_ACTIVE = 0;
    public const ACTIVE = 1;
    public const DELETED = 2;

    // Статусы ответа сервера

    /**
     * @param int $status
     * @return array
     */
    public static function get(int $status = 0): array
    {
        return [
            [
                'status' => 'Something went wrong',
                'code' => 400
            ],
            [
                'status' => 'ok',
                'code' => 200
            ]
        ][$status];
    }

    /**
     * @param string $status
     * @return int
     */
    public static function getNumeric(string $status): int
    {
        return [
            'true' => 1,
            'false' => 0,
        ][$status];
    }

    /**
     * @param string $status
     * @return string
     */
    public static function getDeletedOperator(string $status): string
    {
        return [
            'true' => '!=',
            'false' => '=',
        ][$status];
    }

}
