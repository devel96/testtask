<?php

namespace App\Http\Controllers;

use App\Http\Repositories\IRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController implements RepositoryClassInterface
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected IRepository $repository;

    public function __construct()
    {
        // Полиморфный вызов метода
        $repoClass = $this->repositoryClass();
        $this->repository = new $repoClass();
    }
}
