<?php


namespace App\Providers\V1;


use App\Services\V1\CategoryValidatorService;
use Illuminate\Support\ServiceProvider;

class CategoryValidatorServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->validator->resolver(function ($translator, $data, $rules, $messages) {
            return new CategoryValidatorService($translator, $data, $rules, $messages);
        });
    }
}
