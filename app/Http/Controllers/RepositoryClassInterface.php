<?php


namespace App\Http\Controllers;


interface RepositoryClassInterface
{
    public function repositoryClass(): string;
}
